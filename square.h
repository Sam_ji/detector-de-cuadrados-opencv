#ifndef SQUARE_H
#define SQUARE_H
#include "GL/glut.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <rcdraw.h>

#include <iostream>

using namespace cv;
using namespace std;


class square
{
public:
    square();
    vector<vector<Point> > getSquares();
    void findSquares(vector<vector<Point> > contours);
    double angle( Point pt1, Point pt2, Point pt0 );
    int getSize();

private:
    vector<vector<Point> > squares;

};

#endif // SQUARE_H

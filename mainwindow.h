#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "GL/glut.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <rcdraw.h>
#include "square.h"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QTimer timer; // timer para el slot
    VideoCapture *cap; // objeto para la captura de un frame de video
    RCDraw *visor_source; // visor para la imagen fuente
    RCDraw *visor_target_1; // visor para la imagen procesada 1
    RCDraw *visor_target_2; // visor para la imagen procesada 2
    RCDraw *visor_target_3; // visor para la imagen procesada 3
    QImage *s_img; // Objeto Qimage para la imagen fuente 1
    QImage *t_img_1; // Objeto Qimage para la imagen procesada 1
    QImage *t_img_2; // Objeto Qimage para la imagen procesada 2
    QImage *t_img_3; // Objeto Qimage para la imagen procesada 2
    float cannyThreshmax;
    float cannyThreshmin;
    float sigma;
    int color_detector;
    int color_detecto2;
    int color_detector3;
    square sDetect;



public slots:
        void compute();
private slots:
        void on_horizontalScrollBar_valueChanged(int value);
        void on_horizontalScrollBar_2_valueChanged(int value);
        void on_horizontalScrollBar_3_valueChanged(int value);
        void on_horizontalScrollBar_4_valueChanged(int value);
        void on_horizontalScrollBar_5_valueChanged(int value);
        void on_horizontalScrollBar_6_valueChanged(int value);
};

#endif // MAINWINDOW_H

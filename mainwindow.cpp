/*La implementación de la clase Mainwindow:

   ->Constructor:
   Dibujo de la ventana principal
   Iniciar la cámara
   Crear el objeto imagen fuente  (s_img)
   Crear el objeto visor de la imagen fuente 1 (visor_source)
    Arrancar el connect para el compute
   ->Destructor:

   ->compute (en SLOT) :
   Declarar las matrices imagen Mat:
          captured_image, edges y dst;
   Capturar un frame de una cámara
   Copiar el frame al visor fuente

   Procesar la imagen de la cámara:
            // Captures a frame....
            // Paso a gris
            // binarizes (GaussianBlur and Canny)
            // finds contours (connected components)
            // show contours in randomize color

   Copiar la imagen procesada al visor target 1
   Copiar la imagen procesada al visor target 2
   Copiar la imagen procesada al visor target 3
   Actualizar los cuatro visores.

*/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <string>

// Constructor member
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    cap = new VideoCapture(0); // init object camera 1

    cap->set(CV_CAP_PROP_FRAME_WIDTH,320); // fix width
    cap->set(CV_CAP_PROP_FRAME_HEIGHT,240); // fix heigth


    s_img = new QImage(320,240, QImage::Format_RGB888);

    t_img_1 = new QImage(320,240, QImage::Format_Indexed8);
    t_img_2 = new QImage(320,240, QImage::Format_RGB888);
    t_img_3 = new QImage(320,240, QImage::Format_Indexed8);

    visor_source = new RCDraw(320,240, s_img, ui->SourceFrame);
    visor_target_1 = new RCDraw(320,240, t_img_1, ui->TargetFrame_1);
    visor_target_2 = new RCDraw(320,240, t_img_2, ui->TargetFrame_2);
    visor_target_3 = new RCDraw(320,240, t_img_3, ui->TargetFrame_3);
    connect(&timer,SIGNAL(timeout()),this,SLOT(compute()));
    cannyThreshmax = 1;
    cannyThreshmin = 0;
    sigma = 1.1;
    timer.start(160);
    color_detector = 1;
    color_detecto2 = 255;
    color_detector3 = 1;

}

// Destructor member
MainWindow::~MainWindow()
{
    delete ui;
    delete cap;
    delete s_img;
    delete t_img_1;
    delete t_img_2;
    delete t_img_3;
    delete visor_source;
    delete visor_target_1;
    delete visor_target_2;
    delete visor_target_3;
 }
// implementación del bucle de proceso en SLOT
void MainWindow::compute()
{
   Mat captured_image, edges;

   ////////////////// CAMERA  //////////////////////////
   if(!cap->isOpened())  // check if we succeeded
       exit(-1);
   // get a new frame from camera 1
   *cap >> captured_image;


   // BGR to RGB
   cvtColor(captured_image, captured_image, CV_BGR2RGB,1);
   //copy captured_image_1 to s_img_1 (visor_source_1)
   memcpy(s_img->bits(),captured_image.data,
          captured_image.rows*captured_image.cols*sizeof(uchar)*3 );
   visor_source->update();

   // RGB2GRAY
   cvtColor(captured_image, edges, CV_RGB2GRAY,1);
   // Gaussian Blur
   GaussianBlur(edges, edges,Size(9,9),sigma, sigma);
   memcpy(t_img_3->bits(),edges.data,
          edges.rows*edges.cols*sizeof(uchar) );
   visor_target_3->update();

   // Canny filter (binary)
   Canny(edges, edges, cannyThreshmin, cannyThreshmax, 3);
   Mat dst = Mat::zeros(edges.rows, edges.cols, CV_8UC3); // fill dst to zeros
   edges = edges > 1;
   //copy edges to t_img_1 (visor_target_1)
   memcpy(t_img_1->bits(),edges.data,
          edges.rows*edges.cols*sizeof(uchar) );
   visor_target_1->update();

   ////// Find contours
   vector<vector<Point> > contours;
   vector<Vec4i> hierarchy;
   // finds contours in "edges" image and stores in contours (hierarchy)
   findContours( edges, contours, hierarchy,CV_RETR_TREE, CV_ADAPTIVE_THRESH_GAUSSIAN_C);


   sDetect.findSquares(contours);
   ui->label_12->setText(QString::number(sDetect.getSize()/2));
   polylines(dst,sDetect.getSquares(),true,Scalar(color_detector,color_detecto2,color_detector3),3,8,0);

   //Funcion mágica para combinar dos putas MAT. El resultado de estar buscando una hora :D
   addWeighted(captured_image, 0.5, dst, 0.5, 0.0, dst);
   memcpy(t_img_2->bits(),dst.data,
          dst.rows*dst.cols*sizeof(uchar)*3 );
   visor_target_2->update();






}



void MainWindow::on_horizontalScrollBar_valueChanged(int value)
{
    cannyThreshmax = value;
}

void MainWindow::on_horizontalScrollBar_2_valueChanged(int value)
{
    sigma = value/10.0;
}

void MainWindow::on_horizontalScrollBar_3_valueChanged(int value)
{
    cannyThreshmin = value;
}


void MainWindow::on_horizontalScrollBar_4_valueChanged(int value)
{
    color_detector = value;
}

void MainWindow::on_horizontalScrollBar_5_valueChanged(int value)
{
    color_detecto2 = value;
}

void MainWindow::on_horizontalScrollBar_6_valueChanged(int value)
{
    color_detector3 = value;
}
